package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class GreenDay {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public GreenDay() {
    }
    
    public ArrayList<Song> getGreenDaySongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Bang bang", "Green Day");             			//Create a song
         Song track2 = new Song("Holiday", "Green Day");                        //Create another song
         Song track3 = new Song("American Idiot", "Green Day");                        //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Green Day
         this.albumTracks.add(track2);                                          //Add the second song to song list for Green Day
         this.albumTracks.add(track3);                                          //Add the third song to song list for Green Day
         return albumTracks;                                                    //Return the songs for Green Day in the form of an ArrayList
    }
}
